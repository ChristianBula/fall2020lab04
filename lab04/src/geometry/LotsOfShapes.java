package geometry;

public class LotsOfShapes {
   public static void main(String[] args) {
	   Shape[] s = new Shape[5];
	   s[0] = new Circle(5);
	   s[1] = new Circle(10);
	   s[2] = new Rectangle(5,2);
	   s[3] = new Rectangle(6,2);
	   s[4] = new Square(2);
	   
	   for(int i = 0;i < s.length;i++) {
		   System.out.println("area: "+s[i].getArea());
		   System.out.println("Perimeter: "+s[i].getPerimeter());
	   }
   }
}
