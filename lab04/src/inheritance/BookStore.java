package inheritance;

public class BookStore {

	public static void main(String[] args) {
         Book[] books = new Book[5];
         books[0] = new Book("Doritos","Julian");
         books[1] = new ElectronicBook("Lays","Rodrigo",6);
         books[2] = new Book("Chips","Angela");
         books[3] = new ElectronicBook("EBook","Juan",12);
         books[4] = new ElectronicBook("EBook2","JayC",60);
         
         for(int i = 0;i < books.length;i++) {
        	 System.out.println(books[i]);
        	 System.out.print("");
         }
	}

}
