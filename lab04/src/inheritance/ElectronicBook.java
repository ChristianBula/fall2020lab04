package inheritance;

public class ElectronicBook extends Book{
	private int numberBytes;
	
public ElectronicBook(String title,String author,int numberBytes){
	super(title,author);
	this.numberBytes = numberBytes;
}
public String toString() {
	return "Author: "+this.getAuthor()+" Title: "+this.title+" Size: "+this.numberBytes;
}
}
